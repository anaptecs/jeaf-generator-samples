/*
 * anaptecs GmbH, Ricarda-Huch-Str. 71, 72760 Reutlingen, Germany
 * 
 * Copyright 2021. All rights reserved.
 */
package com.anaptecs.jeaf.accounting.impl.swift.impl;

import com.anaptecs.jeaf.xfun.api.common.ComponentID;

/**
 * Class represents the SWIFTAccounting component.
 *
 * 
 */
public final class SWIFTAccountingComponentConfiguration {
  /**
   * In order to avoid direct instantiation of this class the constructor is set to package visibility.
   */
  SWIFTAccountingComponentConfiguration( ComponentID pComponentID ) {
  }
}
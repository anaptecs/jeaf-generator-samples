package com.anaptecs.jeaf.accounting.impl.domain;

/**
 * @author JEAF Generator
 * @version JEAF Release 1.6.x
 */
public class CompanyBO extends CompanyBOBase {
  /**
   * Initialize object. The constructor of the class has visibility protected in order to avoid creating business
   * objects not through JEAFs persistence service provider.
   */
  protected CompanyBO( ) {
    // Nothing to do.
  }
}